package com.beon.androidchallenge.ui.main

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.GestureDetector
import android.view.GestureDetector.SimpleOnGestureListener
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.beon.androidchallenge.R
import com.beon.androidchallenge.databinding.MainFragmentBinding
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking


class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel
    private lateinit var binding: MainFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = MainFragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(MainViewModel::class.java)

        initViews()
        initObservers()
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun initViews() {
        binding.run {
            numberEditText.addTextChangedListener {
                viewModel.searchNumberFact(it.toString())
            }

            numberEditText.setOnEditorActionListener { textView, actionId, keyEvent ->
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    runBlocking {
                        delay()
                    }
                    this.progressBarCyclic.visibility = View.VISIBLE
                    viewModel.searchNumberFact(textView.text.toString())
                    this.progressBarCyclic.visibility = View.GONE
                    return@setOnEditorActionListener true
                } else {
                    return@setOnEditorActionListener false
                }
            }

            retryButton.setOnClickListener {
                viewModel.searchNumberFact(numberEditText.text.toString())
            }

            numberEditText.setOnTouchListener(OnTouchListener { v, event ->
                gestureDetector.onTouchEvent(event)
            })


            numberEditText.requestFocus()
        }
    }

    private suspend fun delay() {
        delay(3000)
    }

    private fun initObservers() {
        viewModel.currentFact.observeForever {
            if (binding.numberEditText.text.toString().isEmpty()) {
                binding.factTextView.setText(R.string.instructions)
            } else {
                binding.factTextView.text = it?.text
            }
        }
    }

    private val gestureDetector = GestureDetector(context, object : SimpleOnGestureListener() {
        override fun onDoubleTap(e: MotionEvent): Boolean {
            //TODO make the fact favorite
            return true
        }
    })

}